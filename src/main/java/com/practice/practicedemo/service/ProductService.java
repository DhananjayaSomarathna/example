package com.practice.practicedemo.service;

import com.practice.practicedemo.dto.ProductDto;
import com.practice.practicedemo.entity.Product;
import com.practice.practicedemo.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.json.GsonBuilderUtils;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Service
public class ProductService {

    @Autowired
    private ProductRepository productRepository;

    public void saveProduct(ProductDto productDto) {
      Product product = this.ProductDtomapToProduct(productDto);
        productRepository.save(product);
    }

    private Product ProductDtomapToProduct(ProductDto productDto) {
        Product product = new Product();
        product.setProductName(productDto.getProductName());
        product.setPrice(Double.valueOf(productDto.getProductPrice()));
        product.setDescription(productDto.getProductDescription());

        return product;
    }

    public List<Product> getAllProduct() {
        List<Product> products = productRepository.findAll();
        return products;
    }


}
