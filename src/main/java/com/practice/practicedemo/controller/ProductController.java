package com.practice.practicedemo.controller;

import com.practice.practicedemo.dto.ProductDto;
import com.practice.practicedemo.entity.Product;
import com.practice.practicedemo.service.ProductService;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/api")
@RestController
public class ProductController {

    @Autowired
    private ProductService productService;

    @PostMapping("/save")
    @CrossOrigin(origins ="http://localhost:4200/")
    public void saveProduct(@RequestBody ProductDto productDto){
        productService.saveProduct(productDto);
    }

    @GetMapping("/get")
    @CrossOrigin(origins ="http://localhost:4200/")
    public List<Product> getAllProduct(){
        return productService.getAllProduct();
    }
}
